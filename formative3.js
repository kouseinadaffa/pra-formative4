"use strict";

const buah = ['Apel', 'Nanas', 'Pisang', 'Jambu', 'Durian', 'Mangga'];

// for loop
for (let i = 0; i < buah.length; i++) {
    const element = buah[i];
    // console.log(`Buah ke-${i}:  ${element}`);
}

// for in
for (const namaBuah in buah) {
    if (Object.hasOwnProperty.call(buah, namaBuah)) {
        const element = buah[namaBuah];
        // console.log(`Buah ke-${namaBuah}:  ${element}`);
    }
}

// foreach
buah.forEach((element, index) => {
    console.log(`Buah ke-${index}:  ${element}`);
});